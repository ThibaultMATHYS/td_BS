import requests
from bs4 import BeautifulSoup
import re
import csv

# url = "https://docs.python.org/3/"
# r = requests.get(url)
# soup = BeautifulSoup(r.text)
# print(soup)
# print(soup2.find('p'))
""" selection selon balise """
# print(soup.p)
# print(soup.find_all('p'))
""" selection par regex """
# print(soup.find(re.compile(r'..')))
""" selection selon une classe de balise"""
# print(soup.select('.biglink'))
""" selection par fonction """
""" retourne les balise dont le nom ne comporte que 2 cractere"""


# def f(tag):
#    return len(tag.name) == 2


# print(soup.find_all(f))

""" selection par nom de balise + valeur d'un attribut """
# print(soup.find('a', href=re.compile(r'python')))


""" ex1 """
"""
UNI = requests.get("http://www.univ-orleans.fr")
print(UNI.text)
"""

""" ex2 """
"""
UNI = requests.get("http://www.univ-orleans.fr")
soup = BeautifulSoup(UNI.text, 'lxml')
print(soup.h1)
print(soup.find_all('div'))
print(soup.find_all('a'))
print(soup.find_all('img'))
print(soup.find_all('div', class_='composite-zone'))
"""

""" ex3 """
"""
UNI = requests.get("https://stackoverflow.com/questions/tagged/beautifulsoup")
soup = BeautifulSoup(UNI.text, 'lxml')

liens = soup.find_all('a', class_='question-hyperlink', limit=10)
votes = soup.find_all('span', class_='vote-count-post', limit=10)
answers = soup.find_all('div', class_='status', limit=10)

data1 = []
data2 = []
data3 = []

i = 0
while i < 10:
    x = liens[i].get_text()
    y = (votes[i].find('strong', string=re.compile("-?[0-9]*"))
         ).find(string=re.compile("^-?[0-9]*$"))
    z = (answers[i].find('strong', string=re.compile(
        "[0-9]*"))).find(string=re.compile("^[0-9]*$"))
    data1.append(x)
    data2.append(y)
    data3.append(z)
    i += 1

j = 0
file = open('data.csv', 'w')
while j < 10:
    writer = csv.writer(file, delimiter='|')
    writer.writerow((data1[j], data2[j], data3[j]))
    j += 1

file.close()
"""
""" Ex4 """
"""
data = {"submit-form": "", "catalog": "catalogue-2015-2016", "degree": "DP"}
r = requests.post(
    "http://formation.univ-orleans.fr/fr/formation/rechercher-une-formation.html#nav", data=data)
formations = []
soup = BeautifulSoup(r.text, "lxml")
for n in soup.find_all('li', class_='hit'):
    for formation in n.findAll('strong'):
        formations.append(''.join(formation.findAll(text=True)))
print(formations)
"""

""" Ex5 """


def get_definition(x):
    URL = 'http://services.aonaware.com/DictService/Default.aspx?action=define&dict=wn&query={0}'.format(
        x)
    html = requests.get(URL).text
    soup = BeautifulSoup(html, "lxml")
    definitions = []
    for node in soup.find_all('pre', attrs={"class": None}):
        definitions.append(''.join(node.findAll(text=True)))
    return definitions


lines = []
with open('vocabulary.txt') as f:
    lines = f.readlines()

with open('definitions.txt', 'w') as d:
    for mot in lines:
        d.writ
